﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class adegan2soundmanagerscript : MonoBehaviour
{
    public static AudioClip adegan2greeting,adegan2true,adegan2false,seleseSound,akhirSound,sheepSound;
    static AudioSource audioSrc;

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();

        adegan2greeting = Resources.Load<AudioClip>("adegan2greeting");
        adegan2true = Resources.Load<AudioClip>("adegan2true");
        adegan2false = Resources.Load<AudioClip>("adegan2false");
        seleseSound = Resources.Load<AudioClip>("selese");
        akhirSound = Resources.Load<AudioClip>("akhir");
        sheepSound = Resources.Load<AudioClip>("sheep");

        audioSrc = GetComponent<AudioSource>();

    }

    void Update()
    {

    }

    public void greeting()
    {   

        audioSrc.PlayOneShot(adegan2greeting);
        audioSrc.PlayOneShot(sheepSound);
    }

    public void adegantrue()
    {   
        audioSrc.PlayOneShot(adegan2true);
        audioSrc.PlayOneShot(sheepSound);
    }

    public void adeganfalse()
    {   
        audioSrc.PlayOneShot(adegan2false);
        audioSrc.PlayOneShot(sheepSound);
    }
    
    public void selese()
    {   
        audioSrc.PlayOneShot(seleseSound);
        audioSrc.PlayOneShot(sheepSound);
    }

    public void akhir()
    {   
        audioSrc.PlayOneShot(akhirSound);
    }
}
