﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class adegan1scenemanager : MonoBehaviour
{
    public Button myButtonSing;
    public Button myButtonDance;
    public Button myButtonAkro;
    
    private Animator animatorkarakter;
    private Animator animatordomba;
    
    public GameObject karakter;
    public GameObject domba;

    private Button btnAkro,btnDance,btnSing;

    public bool statusDance,statusSing,statusAkro,statusAkhir;

    public AudioSource bgsound;
    public GameObject adegan1;
    public GameObject adegan2;
    public GameObject adegan3;

    void Start()
    {
        animatorkarakter = karakter.GetComponent<Animator> ();
        animatordomba = domba.GetComponent<Animator> ();


        myButtonSing.gameObject.SetActive(false);
        myButtonDance.gameObject.SetActive(false);
        myButtonAkro.gameObject.SetActive(false);

        statusDance = false;
        statusSing = false;
        statusAkro = false;

        statusAkhir = false;

        bgsound = adegan1.GetComponent<AudioSource>();
    }

    void Update()
    {   

        if(animatorkarakter.GetCurrentAnimatorStateInfo(0).IsName("idle"))
        {
            myButtonSing.gameObject.SetActive(true);
            myButtonDance.gameObject.SetActive(true);
            myButtonAkro.gameObject.SetActive(true);
            
            myButtonSing.interactable = true;
            myButtonDance.interactable = true;
            myButtonAkro.interactable = true;
        }

        else if(statusDance==true && statusAkro==true && statusSing==true && statusAkhir==false)
        {
            myButtonSing.interactable = false;
            myButtonDance.interactable = false;
            myButtonAkro.interactable = false;
            animatorkarakter.Play("selese"); 
            animatordomba.Play("selese");         
        }
        else if(statusDance==true && statusAkro==true && statusSing==true && statusAkhir==true)
        {
            animatorkarakter.Play("akhir");
            animatordomba.Play("akhir");
        }
        if(animatorkarakter.GetCurrentAnimatorStateInfo(0).IsName("akhir"))
        {
            myButtonSing.gameObject.SetActive(false);
            myButtonDance.gameObject.SetActive(false);
            myButtonAkro.gameObject.SetActive(false);

            bgsound.enabled = false;
        }
    }

    IEnumerator delay()
    {
        yield return new WaitForSeconds(10);
    }

    public void selesetoakhir()
    {
        statusAkhir = true;
    }

    public void statusdance()
    {
        statusDance = true;
    }

    public void statussing()
    {
        statusSing = true;
    }

    public void statusakro()
    {
        statusAkro = true;
    }


    public void statusawalakro()
    {
        myButtonSing.interactable = false;
        myButtonDance.interactable = false;
        myButtonAkro.interactable = false;
    }
    public void statusakhirakro()
    {
        myButtonSing.interactable = true;
        myButtonDance.interactable = true;
        myButtonAkro.interactable = true;
    }

    
    public void statusawaldance()
    {
        myButtonSing.interactable = false;
        myButtonDance.interactable = false;
        myButtonAkro.interactable = false;
    }
    public void statusakhirdance()
    {
        myButtonSing.interactable = true;
        myButtonDance.interactable = true;
        myButtonAkro.interactable = true;
    }
    
    
    public void statusawalsing()
    {
        myButtonSing.interactable = false;
        myButtonDance.interactable = false;
        myButtonAkro.interactable = false;
    }
    public void statusakhirsing()
    {
        myButtonSing.interactable = true;
        myButtonDance.interactable = true;
        myButtonAkro.interactable = true;
    }

    public void statusgreeting()
    {
        adegan1.SetActive(true);
        adegan2.SetActive(false);
        adegan3.SetActive(false);
    }

    public void statusakhir()
    {
        adegan1.SetActive(false);
        adegan2.SetActive(true);
        adegan3.SetActive(true);
    }

    public void playAnimSing(){
		animatorkarakter.Play("sing");
        animatordomba.Play("sing");
	}

    public void playAnimDance(){
		animatorkarakter.Play("dance");
        animatordomba.Play("dance");
	}

    public void playAnimAkro(){
		animatorkarakter.Play("akro");
        animatordomba.Play("akro");
	}
}
