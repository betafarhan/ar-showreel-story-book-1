﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class adegan1soundmanagerscript : MonoBehaviour
{
    public static AudioClip greetingSound,idleSound,singSound,danceSound,akroSound,seleseSound,akhirSound;
    static AudioSource audioSrc;

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();

        greetingSound = Resources.Load<AudioClip>("greeting");
        idleSound = Resources.Load<AudioClip>("idle");
        singSound = Resources.Load<AudioClip>("sing");
        danceSound = Resources.Load<AudioClip>("dance");
        akroSound = Resources.Load<AudioClip>("akro");
        seleseSound = Resources.Load<AudioClip>("selese");
        akhirSound = Resources.Load<AudioClip>("akhir");

        audioSrc = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void greeting()
    {   

        audioSrc.PlayOneShot(greetingSound);
    }

    public void idle()
    {   
        audioSrc.PlayOneShot(idleSound);
    }

    public void sing()
    {   
        audioSrc.PlayOneShot(singSound);
    }

    public void dance()
    {   
        audioSrc.PlayOneShot(danceSound);
    }

    public void akro()
    {   
        audioSrc.PlayOneShot(akroSound);
    }
    
    public void selese()
    {   
        audioSrc.PlayOneShot(seleseSound);
    }

    public void akhir()
    {   
        audioSrc.PlayOneShot(akhirSound);
    }
}
