﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button_Manager : MonoBehaviour
{
	//ini buat load scene
    public void LoadScene(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }
//ini buat quit game
    public void QuitGame()
    {
        Application.Quit();
    }

    //if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
}
