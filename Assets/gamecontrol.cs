﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gamecontrol : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    //public GameObject panelwinText;
    //public GameObject puzzle1;
    public GameObject[] ObjectsList; // Not only 18, this script will except any number of game objects you put here.
    public static bool locked;
    private Vector2 initialPosition;

    public GameObject eventsy;
    //public GameObject puzz1;

    //public static AudioClip trueSound,falseSound;
    static AudioSource audioSrc;
    

    void Start()
    {

        //trueSound = Resources.Load<AudioClip>("sheep");
        //falseSound = Resources.Load<AudioClip>("akhir");

        audioSrc = GetComponent<AudioSource>();

        //panelwinText.SetActive(false);
        ObjectsList[Random.Range(0,ObjectsList.Length)].SetActive(true);
        //puzzle1
        ayam.locked = false;
        bebek.locked = false;
        banteng.locked = false;

        //puzzle2
        beruang.locked = false;
        buaya.locked = false;
        burunghantu.locked = false;

        //puzzle3
        grizly.locked = false;
        kambing.locked = false;
        kangguru.locked = false;

        //puzzle4
        koala.locked = false;
        kucing.locked = false;
        kudanil.locked = false;

        //puzzle5
        musang.locked = false;
        panther.locked = false;
        pelikan.locked = false;

        //puzzle6
        serigala.locked = false;
        singa.locked = false;
        tikus.locked = false;

        //puzzle7
        badak.locked = false;
        ular.locked = false;
        ulatbulu.locked = false;

        //puzzle8
        tikus.locked = false;
        grizly.locked = false;
        pelikan.locked = false;

        //puzzle9
        singa.locked = false;
        bebek.locked = false;
        kambing.locked = false;

        //puzzle10
        ayam.locked = false;
        panther.locked = false;
        serigala.locked = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(ayam.locked == true && bebek.locked == true && banteng.locked ==true)
        {
            delayy();
        }
        
        if(buaya.locked == true && beruang.locked == true && burunghantu.locked ==true)
        {
            delayy();
        }

        if(grizly.locked == true && kambing.locked == true && kangguru.locked ==true)
        {
            delayy();
        }

        if(koala.locked == true && kucing.locked == true && kudanil.locked ==true)
        {
            delayy();
        }

        if(musang.locked == true && panther.locked == true && pelikan.locked ==true)
        {
            delayy();
        }

        if(serigala.locked == true && singa.locked == true && tikus.locked ==true)
        {
            delayy();
        }

        if(badak.locked == true && ular.locked == true && ulatbulu.locked ==true)
        {
            delayy();
        }

        if(tikus.locked == true && grizly.locked == true && pelikan.locked ==true)
        {
            delayy();
        }

        if(singa.locked == true && bebek.locked == true && kambing.locked ==true)
        {
            delayy();
        }

        if(ayam.locked == true && panther.locked == true && serigala.locked ==true)
        {
            delayy();
        }
    }

    public void LoadScene(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }

    IEnumerator berhenti()
    {
        audioSrc.Play();
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void delayy()
    {             
        StartCoroutine(berhenti());

    }
}
