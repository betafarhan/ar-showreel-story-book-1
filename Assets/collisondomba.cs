﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisondomba : MonoBehaviour
{
    private Animator animatorkarakter2;
    private Animator animatordomba2;
    
    public GameObject karakter2;
    public GameObject domba2;

    void Start()
    {
        animatorkarakter2 = karakter2.GetComponent<Animator> ();
        animatordomba2 = domba2.GetComponent<Animator> ();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Brush")
        {
            Destroy(col.gameObject);
            
            animatorkarakter2.Play("true"); 
            animatordomba2.Play("true");     
        }
    }
}
