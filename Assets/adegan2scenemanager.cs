﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class adegan2scenemanager : MonoBehaviour
{
    public Button myButtonBrush;
    
    private Animator animatorkarakter2;
    private Animator animatordomba2;
    
    public GameObject karakter2;
    public GameObject domba2;

    private Button btnBrush;

    public bool statusTrue,statusAkhir;

    public AudioSource bgsound;

    public GameObject brush;
    public GameObject panelShow;

    public GameObject adegan1;
    public GameObject adegan2;
    public GameObject adegan3;

    void Start()
    {
        animatorkarakter2 = karakter2.GetComponent<Animator> ();
        animatordomba2 = domba2.GetComponent<Animator> ();

        myButtonBrush.gameObject.SetActive(false);

        statusTrue = false;
        statusAkhir = false;

        bgsound = adegan2.GetComponent<AudioSource>();
    }

    void Update()
    {   

        if(animatorkarakter2.GetCurrentAnimatorStateInfo(0).IsName("idle"))
        {
            myButtonBrush.gameObject.SetActive(true);
            myButtonBrush.interactable = true;
        }

        else if(statusTrue==true && statusAkhir==false)
        {
            myButtonBrush.interactable = false;

            animatorkarakter2.Play("brush"); 
            animatordomba2.Play("brushdomba");         
        }
        else if(statusTrue==true && statusAkhir==true)
        {
            animatorkarakter2.Play("akhir");
            animatordomba2.Play("akhir");
        }
        if(animatorkarakter2.GetCurrentAnimatorStateInfo(0).IsName("akhir"))
        {
            myButtonBrush.gameObject.SetActive(false);
            bgsound.enabled = false;
        }
    }

    public void Brush()
    {
        brush.SetActive(true);
        panelShow.SetActive(true);
        myButtonBrush.interactable = false;
    }

    public void paneltutup()
    {
        panelShow.SetActive(false);
    }

    public void statusgreeting()
    {
        adegan1.SetActive(false);
        adegan2.SetActive(true);
        adegan3.SetActive(false);
    }

    // public void statusTrue()
    // {
    //     statusTrue = true;
    // }

    public void statusakhir()
    {
        adegan1.SetActive(true);
        adegan2.SetActive(false);
        adegan3.SetActive(true);
    }

}
