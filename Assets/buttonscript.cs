﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class buttonscript : MonoBehaviour , IPointerDownHandler, IPointerUpHandler
{
    
    public GameObject kubus;
    public float turnSpeed;

    //bool canRotate = false;

    bool ispressed = false;

    void Start()
    {
        
    }

    void Update()
    {
        // if(canRotate)
        // kubus.transform.Rotate(Vector3.up * turnSpeed * Time.deltaTime);

        if (ispressed)
        kubus.transform.Rotate(Vector3.up * turnSpeed * Time.deltaTime);
        return;
    }

    // public void putarKubus()
    // {
    //     canRotate = !canRotate;
    // }
 
    public void OnPointerDown(PointerEventData eventData)
    {
        ispressed = true;
    }
    
    public void OnPointerUp(PointerEventData eventData)
    {
        ispressed = false;
    }
}
