﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class adegan3scenemanager : MonoBehaviour
{
    public Button myButtonFalse1;
    public Button myButtonFalse2;
    public Button myButtonTrue;
    
    private Animator animatorkarakter3;
    private Animator animatordomba3;
    
    public GameObject karakter3;
    public GameObject domba3;

    public bool statusTrue,statusAkhir;

    public AudioSource bgsound;

    public GameObject adegan1;
    public GameObject adegan2;
    public GameObject adegan3;

    void Start()
    {
        animatorkarakter3 = karakter3.GetComponent<Animator> ();
        animatordomba3 = domba3.GetComponent<Animator> ();

        statusTrue = false;
        statusAkhir = false;

        bgsound = adegan3.GetComponent<AudioSource>();

        myButtonFalse1.gameObject.SetActive(false);
        myButtonFalse2.gameObject.SetActive(false);
        myButtonTrue.gameObject.SetActive(false);
    }

    void Update()
    {   

        if(animatorkarakter3.GetCurrentAnimatorStateInfo(0).IsName("idle"))
        {
            myButtonFalse1.gameObject.SetActive(true);
            myButtonFalse2.gameObject.SetActive(true);
            myButtonTrue.gameObject.SetActive(true);
            
            myButtonFalse1.interactable = true;
            myButtonFalse2.interactable = true;
            myButtonTrue.interactable = true;
        }

        if(animatorkarakter3.GetCurrentAnimatorStateInfo(0).IsName("akhir"))
        {
            
            bgsound.enabled = false;
        }
    }

    public void paneltutup()
    {
    }

    public void statusgreeting()
    {
        adegan1.SetActive(false);
        adegan2.SetActive(false);
        adegan3.SetActive(true);
    }

    // public void statusTrue()
    // {
    //     statusTrue = true;
    // }

    public void statusakhir()
    {
        adegan1.SetActive(true);
        adegan2.SetActive(true);
        adegan3.SetActive(false);
    }

    public void interactableButton()
    {
        myButtonFalse1.interactable = false;
        myButtonFalse2.interactable = false;
        myButtonTrue.interactable = false;
    }

    public void playAnimFalse(){
		animatorkarakter3.Play("false");
        animatordomba3.Play("false");
	}

    public void playAnimTrue(){
		animatorkarakter3.Play("true");
        animatordomba3.Play("true");
	}

}
