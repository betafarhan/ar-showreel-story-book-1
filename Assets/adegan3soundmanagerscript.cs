﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class adegan3soundmanagerscript : MonoBehaviour
{
    public static AudioClip adegan3greeting,idleSound,adegan3true,adegan3false,seleseSound,akhirSound,sheepSound;
    static AudioSource audioSrc;

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();

        adegan3greeting = Resources.Load<AudioClip>("adegan3-greeting");
        idleSound = Resources.Load<AudioClip>("adegan3-idle");
        adegan3true = Resources.Load<AudioClip>("adegan3-true");
        adegan3false = Resources.Load<AudioClip>("adegan3-false");
        seleseSound = Resources.Load<AudioClip>("adegan3-selese");
       
        sheepSound = Resources.Load<AudioClip>("sheep");

        audioSrc = GetComponent<AudioSource>();

    }

    void Update()
    {

    }

    public void greeting()
    {   

        audioSrc.PlayOneShot(adegan3greeting);
        audioSrc.PlayOneShot(sheepSound);
    }

    public void idle()
    {   

        audioSrc.PlayOneShot(idleSound);
        audioSrc.PlayOneShot(sheepSound);
    }


    public void adegantrue()
    {   
        audioSrc.PlayOneShot(adegan3true);
        audioSrc.PlayOneShot(sheepSound);
    }

    public void adeganfalse()
    {   
        audioSrc.PlayOneShot(adegan3false);
        audioSrc.PlayOneShot(sheepSound);
    }
    
    public void selese()
    {   
        audioSrc.PlayOneShot(seleseSound);
        audioSrc.PlayOneShot(sheepSound);
    }

}
